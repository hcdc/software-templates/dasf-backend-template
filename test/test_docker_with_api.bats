# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

#  test the created docker setup in the gitlab ci rendered api module

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test docker setup (api)" {
    create_template '{include_docker_setup: "yes", render_api: "yes"}'
    setup_venv
    docker compose  \
      --project-directory $PROJECT_FOLDER \
      -f $PROJECT_FOLDER/docker-compose.dev.yml \
      -f $PROJECT_FOLDER/ci/docker-compose.test.yml \
      up --build --exit-code-from dasf-api
}
