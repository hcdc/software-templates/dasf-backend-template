"""Test module for :mod:`{{ cookiecutter.package_folder}}.api`."""
from typing import Any
from {{ cookiecutter.package_folder }} import api


def test_show_version(connected_module: Any, random_topic: str):
    from {{ cookiecutter.package_folder }} import __version__ as ref_version
    version_info = api.version_info()
    assert "{{ cookiecutter.project_slug }}" in version_info
    assert version_info["{{ cookiecutter.project_slug }}"] == ref_version
